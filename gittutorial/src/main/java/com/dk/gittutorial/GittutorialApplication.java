package com.dk.gittutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GittutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(GittutorialApplication.class, args);
		//adding a comment for the first branch
	}

}
